# [5.0.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.5.6...5.0.0) (2021-10-13)


### Features

* update dependencies ([302def7](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/302def7db90e4a52b98167b5b78a5f852ec7102e))


### BREAKING CHANGES

* major version dependencies updated

## [4.5.6](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.5.5...4.5.6) (2021-03-04)


### Bug Fixes

* change prettier extend ([55e5b61](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/55e5b61cb014a5308cbc535186041b4192ac963d))

## [4.5.5](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.5.4...4.5.5) (2021-03-04)


### Bug Fixes

* update dependencies ([c4d1ef4](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/c4d1ef4e6b566c9a8019bfa9de865ec6f4dbf2ff))

## [4.5.4](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.5.3...4.5.4) (2021-01-31)


### Bug Fixes

* allow PascalCase ([e9063e9](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/e9063e943207d5d62b1e1b0a5a39aafc28c03a5e))

## [4.5.3](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.5.2...4.5.3) (2021-01-12)


### Bug Fixes

* @typescript-eslint/naming-convention ([d16ef08](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/d16ef08744eda3dc2e7b9db923aeadcb5c6bd16d))

## [4.5.2](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.5.1...4.5.2) (2021-01-11)


### Bug Fixes

* disable strict ([bffdd04](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/bffdd0425b97955ab8788f61effc199e51814fd2))

## [4.5.1](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.5.0...4.5.1) (2021-01-09)


### Bug Fixes

* @typescript-eslint/naming-convention ([c5cc7e1](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/c5cc7e19996aaaed87238d97820851a987b402d1))

# [4.5.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.4.0...4.5.0) (2021-01-08)


### Features

* add @typescript-eslint/naming-convention ([e220747](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/e22074765ecf11e4d82c5a7dc0a11023db7683d0))

# [4.4.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.3.1...4.4.0) (2020-12-31)


### Features

* update @typescript-eslint/no-unused-vars ([1cefa17](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/1cefa171c87c483a166e7cc18fffa77a145b639d))

## [4.3.1](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.3.0...4.3.1) (2020-11-30)


### Bug Fixes

* remove parser rules ([26769c4](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/26769c4dd957239148aa5ea9c076decd45ca5c30))

# [4.3.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.2.0...4.3.0) (2020-11-30)


### Features

* add typescript-eslint rules ([d59a0f4](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/d59a0f467c4679ce656fcac94a306bbf54e7ee8a))

# [4.2.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.1.8...4.2.0) (2020-11-22)


### Features

* update dependencies ([8cb5b6f](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/8cb5b6f7fc296801ee98f6336b6efb8393c463be))

## [4.1.8](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.1.7...4.1.8) (2020-11-16)


### Bug Fixes

* remove require-atomic-updates ([e0771ce](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/e0771ced1fb2637f50f2446599d69171cf560adc))

## [4.1.7](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.1.6...4.1.7) (2020-11-08)


### Bug Fixes

* update dependencies ([3827028](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/382702871b8ae555389bfb7b050f4d7139405bdf))

## [4.1.6](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.1.5...4.1.6) (2020-11-08)


### Bug Fixes

* no-use-before-define ([25116b2](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/25116b27bf63e85311151bf2346eca18ac38e997))

## [4.1.5](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.1.4...4.1.5) (2020-10-02)


### Reverts

* Revert "fix: remove prettier" ([f7be4b4](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/f7be4b430b620f78d184ff8f4babd1cba47ec64a))

## [4.1.4](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.1.3...4.1.4) (2020-10-02)


### Bug Fixes

* remove prettier ([2709331](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/270933170eea9154c78cc6d04378fc7b78e20d74))

## [4.1.3](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.1.2...4.1.3) (2020-10-01)


### Bug Fixes

* remove require-await ([c201ff8](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/c201ff87d2e18910eb52fdd0276f9a3cfb36e29c))

## [4.1.2](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.1.1...4.1.2) (2020-09-30)


### Bug Fixes

* no-use-before-define ([f8da63e](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/f8da63e2c3360c036374f79f475cea5c3bd4b9c9))

## [4.1.1](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.1.0...4.1.1) (2020-09-30)


### Bug Fixes

* remove jest rules ([361cdad](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/361cdad673d0f92d1c99dd07e96774d9480792c2))

# [4.1.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.0.1...4.1.0) (2020-09-11)


### Features

* update dependencies and new rules ([406eaf7](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/406eaf7324476ad64a51d78ac0b52dafcb65cbaa))

## [4.0.1](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/4.0.0...4.0.1) (2020-06-29)


### Bug Fixes

* update dependencies ([81c62bd](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/81c62bdbce2cb007372b5c7c065c84a3af38a88f))

# [4.0.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/3.0.1...4.0.0) (2020-06-02)


### Features

* update dependencies ([9f5e4e3](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/9f5e4e396057f0e238fe3e2c82bfd97a9460256e))


### BREAKING CHANGES

* update to ESLint 7

## [3.0.1](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/3.0.0...3.0.1) (2020-03-28)


### Bug Fixes

* Invalid confif ([00259a4](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/00259a4a9df101b1acfff4dcc30eadc1473e1459))

# [3.0.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/2.2.0...3.0.0) (2020-03-27)


### Features

* update dependencies ([7a138f8](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/7a138f830572cd6c3cb00662450c991006de2b60))


### BREAKING CHANGES

* Require Node.js >= 10.18

# [2.2.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/2.1.1...2.2.0) (2020-03-17)


### Features

* Update dependencies ([e4273af](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/e4273afb9c217096a29e2faacd0243e35f3964f3))

## [2.1.1](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/2.1.0...2.1.1) (2019-11-12)


### Bug Fixes

* update typescript-eslint ([cd9c38b](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/cd9c38b6b70471dbef6e9280f73cbbe13e682b9e))

# [2.1.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/2.0.0...2.1.0) (2019-11-09)


### Features

* support TypeScript 3.7 ([4759204](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/4759204ce53c346b6815c68c6961466ce32cb384))

# [2.0.0](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/1.2.4...2.0.0) (2019-08-23)


### Features

* Allow high order functions without return type ([72bb91c](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/72bb91c))
* Update dependencies ([378535a](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/378535a))


### BREAKING CHANGES

* Drop Node 6 support

## [1.2.4](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/1.2.3...1.2.4) (2019-07-31)


### Bug Fixes

* Fix overrides file ([d2ed34e](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/d2ed34e))

## [1.2.3](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/1.2.2...1.2.3) (2019-07-31)


### Bug Fixes

* no-dupe-class-members ([2ea0d7d](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/2ea0d7d))

## [1.2.2](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/1.2.1...1.2.2) (2019-07-22)


### Bug Fixes

* **ci:** Fix semantic-release plugins order ([896e328](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/896e328))

## [1.2.1](https://gitlab.com/joshua-avalon/eslint-config-typescript/compare/1.2.0...1.2.1) (2019-07-22)


### Bug Fixes

* **ci:** GitLab CI does not run correctly ([6557230](https://gitlab.com/joshua-avalon/eslint-config-typescript/commit/6557230))
