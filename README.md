# ESLint Config Typescript

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Shareable ESLint Typescript config.

## Installation

```
npm i -D @joshuaavalon/eslint-config-typescript
```

## Usage

**.eslintrc.yaml**

```yaml
extends:
  - "@joshuaavalon/eslint-config-typescript"
```

### Use with VS Code

Add the following to `.vscode/settings.json` if you use ESLint extension with VS Code.

```json
{
  "editor.formatOnSave": true,
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    "typescript",
    "typescriptreact"
  ],
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  }
}
```

[license]: https://gitlab.com/joshua-avalon/eslint-config-typescript/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/joshua-avalon/eslint-config-typescript/pipelines
[pipelines_badge]: https://gitlab.com/joshua-avalon/eslint-config-typescript/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@joshuaavalon/eslint-config-typescript
[npm_badge]: https://img.shields.io/npm/v/@joshuaavalon/eslint-config-typescript/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
